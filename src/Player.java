
public class Player {
	private char name;
	private int win;
	private int lose;
	private int draw;
	
	Player (char name){
		this.name=name;
		win=0;
		lose=0;
		draw=0;
	}
	public char getname() {
		return name;
	}
	public int getwin() {
		return win;
	}
	public int getlose() {
		return lose;
	}
	public int getdraw() {
		return draw;
	}
	public void win(){
		win++;
	}
	public void lose() {
		lose++;
	}
	public void draw() {
		draw++;
	}
}
